class_name Item
extends Node


export (String) var item_name = "Item"
export (String) var description = "This is an item."
export (int) var value = 0
export (float) var weight = 0
export (bool) var is_quest_item = false
export (bool) var consumable = false
export (bool) var equipable = false


# Return true if the player can pick up the item
func player_can_pickup(area):
	var player_inv = area.get_parent().inventory
	return player_inv.weight + weight <= player_inv.max_weight


# Add the item to the player's inventory
func pickup_item(area):
	var player_inv = area.get_parent().inventory
	player_inv.add_item(self, 1)
	return true


# An generic pickup function
func pickup(area):
	#if player_can_pickup(area):
	pickup_item(area)
	queue_free()
