class_name LootEntry
extends Node

export (PackedScene) var item
export (float) var drop_chance
export (Array, int) var quantities

func _init(a_item, a_drop_chance, a_quantities):
	self.item = a_item
	self.drop_chance = a_drop_chance
	self.quantities = a_quantities
