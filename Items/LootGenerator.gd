class_name LootGenerator
extends Node2D
# A utility class to generate loot, commonly used by mobs and chests


# Return a list of loot to drop, sourced from loot_table (an Array of LootEntry)
func generate_loot(loot_table):
	var loot = []
	for loot_entry in loot_table:
		if randf() <= loot_entry.drop_chance:
			var quantity = int(rand_range(loot_entry.quantities[0], loot_entry.quantities[1]))
			loot.append([loot_entry.item, quantity])
	return loot
