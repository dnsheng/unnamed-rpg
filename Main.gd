extends Node


onready var playerChar = $PlayerChar

# A variable is needed because otherwise PlayerChar always ends up at (0,0)
var _spawn_position = Vector2(30, 75)
var level = null

# Called when the node enters the scene tree for the first time.
func _ready():
	# Get the level child (identified by 'level' group)
	level = get_tree().get_nodes_in_group("level")[0]
	register_level()


# Register a new level to the game
func register_level():
	# Add PlayerChar to level's PlayerRef (RemoteTransform)
	level.playerRef.remote_path = level.playerRef.get_path_to(playerChar)
	# Move the player to the new position (aka moving RemoteTransform)
	level.playerRef.position = _spawn_position
	connect_level_signals()


# Connect all signals in the level to the main game
func connect_level_signals():
	# Connect signals for LTA level changes
	for lta in level.ltaAreas:
		if not lta.is_connected("change_level", self, "change_level"):
			lta.connect("change_level", self, "change_level")


# Change the current level with a new level
func change_level(dest_level, dest_position):
	call_deferred("_deferred_change_level", dest_level, dest_position)


func _deferred_change_level(dest_level, dest_position):
	# Save the dest_position in the class attribute
	_spawn_position = dest_position
	# 'queue_free' automatically disconnects signals
	level.queue_free()
	# Load, instantiate, and register new level
	level = load(dest_level).instance()
	add_child(level)
	register_level()
