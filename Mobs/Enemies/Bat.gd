class_name Bat
extends "res://Mobs/Enemies/EnemyMob.gd"


onready var animatedSprite = $AnimatedSprite

func _ready():
	loot_table = [LootEntry.new("res://Items/Food/Bug.tscn", 1, [1,4]),]


func _physics_process(delta):
	process_animations()
	._physics_process(delta)


func process_animations():
	animatedSprite.flip_h = velocity.x < 0
