class_name EnemyMob
extends "res://Mobs/Generic/Mob.gd"
# This implements generic behaviour of enemy mobs
# TODO: This is basically a melee mob, refactor for ranged mobs


export (int) var WANDER_TARGET_RANGE = 4

onready var detectionBox = $DetectionBox
onready var wanderController = $WanderController


func _physics_process(delta):
	process_state()
	match state:
		IDLE:
			idle_state(delta)
		WANDER:
			wander_state(delta)
		CHASE:
			chase_state(delta)

	process_movement(delta)


# Selects the appropriate state for the EnemyMob
func process_state():
	# If a body is detected, start to chase
	if detectionBox.target_found:
		state = CHASE
	# If a new wander target is available, either wander or idle
	elif wanderController.get_time_left() == 0:
		state = pick_random_state([IDLE, WANDER])
		wanderController.start_wander_timer(rand_range(1, 3))
	# Otherwise, state is maintained


func idle_state(delta):
	velocity = velocity.move_toward(Vector2.ZERO, FRICTION * delta)


func wander_state(delta):
	# Move towards the wander target position
	var wander_dir = global_position.direction_to(wanderController.target_position)
	velocity = velocity.move_toward(wander_dir * MAX_SPEED / 2, delta * ACCELERATION)
	# If close enough to position, wander/idle
	if global_position.distance_to(wanderController.target_position) <= WANDER_TARGET_RANGE:
		state = pick_random_state([IDLE, WANDER])
		wanderController.start_wander_timer(rand_range(1, 3))


func chase_state(delta):
	# Start to chase towards body
	velocity = velocity.move_toward(detectionBox.target_direction * MAX_SPEED,
									ACCELERATION * delta)


# Returns a random state from the list
func pick_random_state(state_list):
	return state_list[randi() % state_list.size()]
