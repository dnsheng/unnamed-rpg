extends Area2D


# The scale of the DetectionBox will changed when a body is detected
export (float) var expanded_radius = 1.5

var target_found = false
var target_area = null
var target_direction = Vector2.ZERO
var original_radius = scale


func _physics_process(_delta):
	if target_found:
		target_direction = (target_area.global_position - global_position).normalized()


# Go toward the body that entered the detection radius
func _on_DetectionBox_area_entered(area):
	target_found = true
	target_area = area
	scale = Vector2(expanded_radius, expanded_radius)


# Body exits the detection radius
func _on_DetectionBox_area_exited(_area):
	target_found = false
	target_direction = Vector2.ZERO
	scale = original_radius
