class_name Inventory
extends Node
# Implements an inventory system to hold items.
# Items are stored in a dictionary that maps item filenames to quantity.
# Item filenames are used because item objects are unique.
# Using item objects as a key would cause multiple entries to be stored.
# This may be considered in the future if items have variance.
# All add() and remove() functions expect the actual item object to be passed.


signal apply_status(status)
signal remove_status(status)
signal item_obtained(item, quantity)
signal item_removed(item, quantity)

export (float) var max_weight setget set_max_weight
export (float) var weight setget set_weight

# Maps item filename to quantity
var _inventory_dict = {}
var overencumbered = false


# Set the current weight
func set_weight(value):
	weight = value
	if weight > max_weight:
		print("Inventory says overencumbered (%s/%s)" % [weight, max_weight])
		# Prevent setting overencumbered state multiple times
		if not overencumbered:
			overencumbered = true
			emit_signal("apply_status", "res://Mobs/StatusEffects/OverencumberedStatus.tscn")
	elif overencumbered:
		overencumbered = false
		emit_signal("remove_status", "res://Mobs/StatusEffects/OverencumberedStatus.tscn")


# Set the max weight
func set_max_weight(value):
	# Cannot go below zero
	max_weight = max(0, value)


# Empty the inventory
func empty(include_quest_items=false):
	var item_list = _inventory_dict.keys()
	for item in item_list:
		remove_item_all(item, include_quest_items)


# Removes an item from the inventory and returns it
# Does nothing and returns null if item not in inventory
# If quantity argument exceeds actual quantity, continues to remove all items
func remove_item(item, quantity=1, include_quest_items=false):
	if not _item_removable(item, include_quest_items):
		return null
	# If the quantity arg exceeds actual quantity, adjust quantity arg
	if _inventory_dict[item.filename] < quantity:
		quantity = _inventory_dict[item.filename]
	_inventory_dict[item.filename] -= quantity
	# Instantiate item to get weight
	self.weight -= item.weight * quantity
	emit_signal("item_removed", item, quantity)


# Remove all quantities of 'item' from the inventory
func remove_item_all(item, include_quest_items=false):
	if not _item_removable(item, include_quest_items):
		return null
	var quantity = _inventory_dict[item.filename]
	_inventory_dict.erase(item.filename)
	self.weight -= item.weight * quantity
	emit_signal("item_removed", item, quantity)


# Adds an item to the inventory
func add_item(item, quantity=1):
	# Store item's filename as a key
	_inventory_dict[item.filename] = item_quantity(item) + quantity
	self.weight += item.weight
	emit_signal("item_obtained", item, quantity)


# Return true if the item is removable from the inventory
func _item_removable(item, include_quest_items):
	return item_quantity(item) > 0 and (not item.is_quest_item or include_quest_items)


# Returns the quantity of an item in the inventory (zero if item DNE)
func item_quantity(item):
	return _inventory_dict.get(item.filename, 0)
