class_name Mob
extends KinematicBody2D


enum {
	IDLE,
	CHASE,
	WANDER
}

# Time between auto attacks
export (float) var ATTACK_SPEED = 1.0
export (int) var MAX_SPEED = 70
export (int) var FRICTION = 500
export (int) var ACCELERATION = 100
export (int) var KNOCKBACK_POWER = 250
export (int) var KNOCKBACK_DISABLED = false

onready var stats = $Stats
onready var hitboxCollision = $Hitbox/CollisionShape2D
onready var autoAttackTimer = $AutoAttackTimer
onready var softCollision = $SoftCollision
onready var lootGenerator = $LootGenerator

# Should be array containing LootEntry, but currently exporting custom types is unsupported
# Just remember to define the items in _ready() I guess
var loot_table
var velocity = Vector2.ZERO
var knockback = Vector2.ZERO
var state = IDLE


func process_movement(delta):
	# Collision - we need to check every frame (aka as long as there is overlap)
	if softCollision.is_colliding():
		velocity += softCollision.get_push_vector() * ACCELERATION * delta

	# Knockback
	knockback = knockback.move_toward(Vector2.ZERO, FRICTION * delta)
	knockback = move_and_slide(knockback)
	
	# Regular movement
	if knockback == Vector2.ZERO:
		velocity = move_and_slide(velocity)


func _on_Stats_no_health():
	var loot = lootGenerator.generate_loot(loot_table)
	print(loot)
	call_deferred("queue_free")


# Mob takes damage
func _on_Hurtbox_area_entered(area):
	print("%s took %s damage (%s/%s)" % [self.name, area.damage, stats.health, stats.max_health])
	stats.health -= area.damage
	if not KNOCKBACK_DISABLED:
		knockback = area.global_position.direction_to(global_position) * KNOCKBACK_POWER


# Mob deals damage (auto attacks)
func _on_Hitbox_area_entered(_area):
	autoAttackTimer.start(ATTACK_SPEED)
	hitboxCollision.set_deferred("disabled", true)


# Called when mob can auto-attack again
func _on_AutoAttackTimer_timeout():
	hitboxCollision.set_deferred("disabled", false)
