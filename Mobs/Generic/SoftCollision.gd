class_name SoftCollision
extends Area2D
# This collision is for Kinematic2D bodies to prevent body overlap
# On overlap, the bodies will be pushed away from eachother.


func is_colliding():
	return get_overlapping_areas().size() > 0


func get_push_vector():
	var areas = get_overlapping_areas()
	var push_vector = Vector2.ZERO
	if is_colliding():
		var area = areas[0]
		push_vector = area.global_position.direction_to(global_position)
		
	return push_vector
