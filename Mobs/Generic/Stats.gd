class_name Stats
extends Node


signal no_health
signal health_changed(health)
signal max_health_changed(max_health)

signal no_stamina
signal stamina_changed(stamina)
signal max_stamina_changed(max_stamina)

export (float) var max_health = 1 setget set_max_health
export (float) var health = max_health setget set_health

export (float) var max_stamina = 1 setget set_max_stamina
export (float) var stamina = max_stamina setget set_stamina
export (float) var stamina_regen_per_sec = 20
export (float) var stamina_timeout = 1.5

onready var staminaTimer = $StaminaTimer

var _stamina_regen_enabled = false


func _physics_process(delta):
	_regenerate(delta)


# Regenerate stamina
func _regenerate(delta):
	if _stamina_regen_enabled:
		stamina = min(stamina + stamina_regen_per_sec * delta, max_stamina)


# Set the current health, cannot exceed max_health
func set_health(value):
	health = clamp(value, 0, max_health)
	emit_signal("health_changed", health)
	if health <= 0:
		emit_signal("no_health")


# Set the max health, reducing current health if necessary
func set_max_health(value):
	max_health = value
	health = min(health, max_health)
	emit_signal("max_health_changed", max_health)


# Set the current stamina, cannot exceed max_stamina
func set_stamina(value):
	# Add regen timeout on stamina decrease (usage)
	if value < stamina:
		staminaTimer.start(stamina_timeout)
		_stamina_regen_enabled = false

	# Set stamina
	stamina = clamp(value, 0, max_stamina)
	emit_signal("stamina_changed", stamina)

	# Stop regenerating stamina when full
	if stamina == max_stamina:
		_stamina_regen_enabled = false

	if stamina <= 0:
		emit_signal("no_stamina")


# Set the max stamina, reducing current stamina if necessary
func set_max_stamina(value):
	max_stamina = value
	stamina = min(stamina, max_stamina)
	emit_signal("max_stamina_changed", max_stamina)


# Enable stamina regen on timer timeout
func _on_StaminaTimer_timeout():
	_stamina_regen_enabled = true
