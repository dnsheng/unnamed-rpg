class_name Status
extends Node
# Defines a status (buff/debuff) for player/mobs


enum STATUS_TYPE {
	BLEED,
	POISON,
	MAGIC,
}

export (String) var status_name
export (String) var description
export (bool) var is_debuff
export (float) var duration
export (bool) var is_removable = true
export (STATUS_TYPE) var type


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Apply the status to the entity
func apply(_entity):
	pass


# Remove the status from the entity
func remove(_entity):
	pass
