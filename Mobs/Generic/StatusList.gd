class_name StatusList
extends Node
# A wrapper over an array of Status objects
# Functions to easily add/remove/search

var _statuses = []


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Instantiate the status string, apply the status, and add to the list
func add(status: String, target):
	print("StatusList adding %s to %s" % [status, target])
	var status_obj = load(status).instance()
	add_to(status_obj, target)
	print(_statuses)


# Apply the status and add the Status object to the list
func add_to(status: Status, target):
	status.apply(target)
	_statuses.append(status)


# Clear the status effects and remove the status from the list
# 'force' arg overwrite the is_removable attribute in Status
# Does nothing if status DNE or can't be removed
# TODO: What if there are multiple instances of the same status?
func remove(status: String, target, force=false):
	var status_obj = load(status).instance()
	remove_to(status_obj, target, force)


# Clear the status effects and remove the status object from the list
func remove_to(status: Status, target, force=false):
	var index = -1
	# Find the status by comparing status_name
	for st in _statuses:
		if st.status_name == status.status_name and (st.is_removable or force):
			index = _statuses.find(st)
	# If status found, remove status effects and remove from list
	if index != -1:
		status.remove(target)
		_statuses.remove(index)


# Apply all status effects
func apply_all_effects(entity):
	for status in _statuses:
		status.apply(entity)


# Remove all status effects
func remove_all_effects(entity):
	for status in _statuses:
		status.remove(entity)
