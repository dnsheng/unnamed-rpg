class_name WanderController
extends Node2D
# Controls Mob wander behaviour
# Every X seconds sets a new target_destination to wander to


export (int) var wander_range = 32

onready var timer = $Timer

onready var start_position = global_position
onready var target_position = global_position


# Called when the node enters the scene tree for the first time.
func _ready():
	update_target_position()


func update_target_position():
	var target_vector = Vector2(rand_range(-wander_range, wander_range),
								rand_range(-wander_range, wander_range))
	target_position = start_position + target_vector


func get_time_left():
	return timer.time_left


func start_wander_timer(duration):
	timer.start(duration)


func _on_Timer_timeout():
	update_target_position()
