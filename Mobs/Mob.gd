class_name Mob
extends Area2D


class LootEntry:
	var item
	var drop_chance
	var quantities
	
	func _init(item, drop_chance, quantities):
		self.item = item
		self.drop_chance = drop_chance
		self.quantities = quantities


onready var stats = $Stats
var loot_table = [LootEntry.new("res://Items/Food/Bug.tscn", 1, [1,3]),]


# Called when the node enters the scene tree for the first time.
#func _ready():
#	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass



# Generate a list of loot to drop, sourced from loot_table
func _generate_loot():
	var loot = []
	for loot_entry in loot_table:
		if randf() <= loot_entry.drop_chance:
			var quantity = int(rand_range(loot_entry.quantities[0], loot_entry.quantities[1]))
			loot.append([loot_entry.item, quantity])
	return loot


func _on_Stats_no_health():
	var loot = _generate_loot()
	print(loot)
	queue_free()


func _on_Hurtbox_area_entered(area):
	stats.health -= area.damage
	print("Bat took %s damage (%s/%s)" % [area.damage, stats.health, stats.max_health])
