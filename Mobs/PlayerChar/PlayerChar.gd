class_name PlayerChar
extends KinematicBody2D


enum {
	MOVE,
	ROLL,
	ATTACK,
	SPRINT,
	SNEAK
}

# These states only change once the animation finishes - not by input
const IMMUTABLE_STATES = [ATTACK, ROLL]

export (int) var MAX_SPEED = 100
export (int) var ACCELERATION = 500
export (int) var FRICTION = 2000
export (float) var SPRINT_MULTIPLIER = 2
export (float) var OVERENCUMBERED_MULTIPLIER = 2
# Cost of points per frame
export (float) var SPRINT_STAMINA_COST = .5
export (float) var DETECTION_RADIUS = 55
export (float) var SNEAK_RADIUS_MULTIPLIER = .5
export (float) var SNEAK_MOVESPEED_MULTIPLIER = .5

onready var stats = PlayerStats
onready var inventory = PlayerInventory
onready var playerStatusList = PlayerStatusList

onready var hitbox = $SwordHitbox/Hitbox
onready var animationTree = $AnimationTree
onready var animationState = animationTree.get("parameters/playback")
onready var interactPosition = $InteractionPosition
onready var interactCollision = $InteractionPosition/Interaction/CollisionShape2D
onready var detectionShape = $DetectionBox/CollisionShape2D

var state = MOVE setget set_state
var velocity = Vector2.ZERO
var interaction_count = 0


# Called when the node enters the scene tree for the first time.
func _ready():
	# Handling death (temporary)
	stats.connect("no_health", self, "death_state")
	# Handle encumberment
	inventory.connect("apply_status", self, "apply_status")
	inventory.connect("remove_status", self, "remove_status")

	animationTree.active = true
	interactCollision.disabled = true


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	var player_dir = process_input_direction()
	position_animations(player_dir)
	set_interaction_box(player_dir)
	self.state = process_state()

	match state:
		MOVE, SPRINT, SNEAK:
			move_state(delta, player_dir)
		ATTACK:
			attack_state()

	velocity = move_and_slide(velocity)


# Process input to get normalized PlayerChar direction vector
func process_input_direction():
	# Get XY direction vector
	var input_vector = Vector2.ZERO
	input_vector.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	input_vector.y = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")

	return input_vector.normalized()


# Apply the PlayerChar's direction to the defined animation blend spaces
func position_animations(player_dir):
	# Only update if there's a change in direction, otherwise keep as-is
	if player_dir != Vector2.ZERO:
		animationTree.set("parameters/idle/blend_position", player_dir)
		animationTree.set("parameters/run/blend_position", player_dir)
		animationTree.set("parameters/attack/blend_position", player_dir)


# Change PlayerChar state based on input
func process_state():
	# If current state is immutable, do not process state
	if IMMUTABLE_STATES.has(state):
		return state
	# Get current state based on input, default = MOVE
	if Input.is_action_just_pressed("ui_attack"):
		return ATTACK
	if Input.is_action_pressed("ui_sprint") and stats.stamina > 0:
		return SPRINT
	if Input.is_action_pressed("ui_sneak"):
		return SNEAK
	return MOVE


# Position interaction box and enable if PlayerChar attempts to interact
func set_interaction_box(player_dir):
	# Position box if there's movement
	if player_dir != Vector2.ZERO:
		interactPosition.rotation_degrees = (player_dir.angle() * 180 / PI) - 90
	# Enable interaction box if key pressed, disabled by default
	interactCollision.disabled = not Input.is_action_just_pressed("ui_interact")
	# This is really hacky - prevents PlayerChar from interacting with >1 object
	interaction_count = 0


# Execute actions in the move state
func move_state(delta, player_dir):
	var destination = Vector2.ZERO
	var rate = 0

	if player_dir != Vector2.ZERO:
		# Input direction vector given, run towards direction
		animationState.travel("run")
		destination = player_dir.normalized() * MAX_SPEED
		rate  = ACCELERATION

		# Apply sprinting/sneaking multipliers, exclusive
		match state:
			SPRINT:
				stats.stamina -= SPRINT_STAMINA_COST
				destination *= SPRINT_MULTIPLIER
				rate *= SPRINT_MULTIPLIER
			SNEAK:
				destination *= SNEAK_MOVESPEED_MULTIPLIER
				rate *= SNEAK_MOVESPEED_MULTIPLIER

	else:
		# No direction given, idle
		animationState.travel("idle")
		rate = FRICTION
	
	velocity = velocity.move_toward(destination, rate * delta)


# Execute actions in the attack state
func attack_state():
	velocity = Vector2.ZERO
	animationState.travel("attack")


# When the attack animation finishes playing, return to idle state
func _on_attack_finished():
	self.state = MOVE


# Called when state changes
func set_state(new_state):
	# Change detection radius when entering/leaving sneak radius
	if state != new_state:
		if state == SNEAK:
			detectionShape.shape.radius /= SNEAK_RADIUS_MULTIPLIER
		elif new_state == SNEAK:
			detectionShape.shape.radius *= SNEAK_RADIUS_MULTIPLIER
	state = new_state


# Handles death of PlayerChar
func death_state():
	call_deferred("queue_free")


# Called when the player gets damaged
func _on_Hurtbox_area_entered(area):
	print("Player got hurt!")
	stats.health -= area.damage


# Called when the player picks up an object
func _on_ItemPickup_area_entered(area):
	print("Picking up an item %s" % area.name)
	print(inventory._inventory_dict)
	print("%s/%s" % [inventory.weight, inventory.max_weight])


# Called when the player interacts with an object
func _on_Interaction_area_entered(area):
	# Prevent multiple interactions from occuring
	if interaction_count == 0:
		print("Interacting with a %s" % area.name)
		# TODO: This may break
		var object = area.get_parent()
		object.interact([self])
		interaction_count += 1


# Apply a status effect to the PlayerChar
func apply_status(status):
	print("PlayerChar applying status: %s", status)
	playerStatusList.add(status, self)


# Remove a status effect from the PlayerChar
func remove_status(status):
	print("PlayerChar removing status: %s", status)
	playerStatusList.remove(status, self)
