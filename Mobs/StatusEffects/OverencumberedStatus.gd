extends "res://Mobs/Generic/Status.gd"

export (float) var MAX_SPEED_DECREASE = 2
export (float) var ACCELERATION_DECREASE = 2
export (float) var STAMINA_COST_INCREASE = 2


# Apply the effects to the entity
func apply(entity):
	entity.MAX_SPEED /= MAX_SPEED_DECREASE
	entity.ACCELERATION /= ACCELERATION_DECREASE
	entity.SPRINT_STAMINA_COST /= STAMINA_COST_INCREASE


# Remove affects from the entity
func remove(entity):
	entity.MAX_SPEED *= MAX_SPEED_DECREASE
	entity.ACCELERATION *= ACCELERATION_DECREASE
	entity.SPRINT_STAMINA_COST *= STAMINA_COST_INCREASE
