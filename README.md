# Unnamed RPG

Parts of this game were inspired by HeartBeast's guide: [Make an Action RPG in Godot 3.2](https://www.youtube.com/playlist?list=PL9FzW-m48fn2SlrW0KoLT4n5egNdX-W9a)

Written using Godot 3.2


### Assets Used:
+ [HeartBeast - Action RPG Resources](https://github.com/uheartbeast/youtube-tutorials/blob/master/Action%20RPG/Action%20RPG%20Resources.zip)
+ [Henry Software - Free Pixel Food](https://henrysoftware.itch.io/pixel-food)
+ [Limezu - Kitchen](https://limezu.itch.io/kitchen)
