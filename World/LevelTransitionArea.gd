class_name LevelTransitionArea
extends Area2D


signal change_level(dest_level, dest_position)

# The level to transition to
# Using PackedScenes won't work because it instantiates the levels, causing a dependency issue
export (String) var dest_level
# The global_position for the PlayerChar in the new level
export (Vector2) var dest_position = Vector2.ZERO


# When a body (presumably player) enters the LTA, signal for a level change
func _on_LevelTransitionArea_body_entered(_body):
	emit_signal("change_level", dest_level, dest_position)
