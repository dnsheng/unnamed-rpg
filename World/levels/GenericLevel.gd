class_name GenericLevel
extends Node2D


# A reference to the RemoteTransform to project PlayerChar from Main
#onready var playerRef = $EntityYSort/PlayerRef
onready var playerRef = $EnvTileMap/PlayerRef
# An array of all LTAs in the level
onready var ltaAreas = $LTAYSort.get_children()
