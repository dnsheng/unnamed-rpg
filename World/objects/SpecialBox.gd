extends "res://World/objects/TestBox.gd"


func interact(args):
	print("Removing all bugs")
	var player = args[0]
	var bug = load("res://Items/Food/Bug.tscn").instance()
	player.inventory.remove_item_all(bug)
	print(player.inventory._inventory_dict)
